class StreetsController < ApplicationController

  def console
    @streets = Street.all
  end

  def new
    @street = Street.new
  end

  def create
    @street = Street.new(street_params)
    @street.save
    respond_to :js
  end

  def show
    @street = Steet.find(params[:id])
  end

  def edit

  end

  def update

  end

  private

  def street_params
    params.require(:street).permit(:name, :borough, :left_side, :right_side, :start_time, :end_time)
  end

end
