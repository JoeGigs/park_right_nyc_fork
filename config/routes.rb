Rails.application.routes.draw do
  root 'streets#console'
  resources :streets

  get '/streets/new', to: 'streets#new'
  post '/streets/new', to: 'streets#create'
end
