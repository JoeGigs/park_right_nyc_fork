class AddLeftAndRightSideToStreets < ActiveRecord::Migration[5.0]
  def change
  	add_column :streets, :left_side, :string
  	add_column :streets, :right_side, :string
  end
end
